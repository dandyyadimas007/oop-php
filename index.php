<?php
include_once("Animal.php");
include_once("frog.php");
include_once("Ape.php");


$sheep = new Animal("shaun");
echo "Nama hewan : $sheep->name <br>"; // "shaun"
echo "Jumlah kaki : $sheep->legs <br>"; // 2
echo "Berdarah dingin ? $sheep->cold_blooded <br><br>"; // false


$sungokong = new Ape("kera sakti");
echo "Nama hewan : $sungokong->name <br>";
$sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama hewan : $kodok->name <br>";
$kodok->jump() ; // "hop hop"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>